# CockroachDB Swarm

This Docker version of CockroachDB in such a way that it can be used with Docker Swarm's auto-scale functionality.

```yaml
version: "3.3"
services:
  master:
    hostname: "master" # important for referencing the right host
    image: cockroachdb-master
    ports:
      - 8080:8080
      - 26257:26257
      
  slave:
    stop_grace_period: 30s # max time for the slave to quit cluster and exit
    image: cockroachdb-slave
```
