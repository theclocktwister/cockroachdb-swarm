import signal
import time
import sys
import logging
from subprocess import Popen


def exit_gracefully(frame, signum, *args, **kwargs):
    logging.warning(f"Terminating process")  # will print a message to the console
    main_task.terminate()
    main_task.wait()
    logging.warning("CockroachDB has terminated.")

    logging.warning("Sending quit command")
    shutdown_task = Popen(['cockroach', 'quit', '--insecure', '--host=master'], shell=False)
    shutdown_task.wait()

    logging.warning("Command sent. Awaiting kill...")

    for i in range(10):
        logging.warning(i)
        time.sleep(1)
    sys.exit(0)


signal.signal(signal.SIGINT, exit_gracefully)
signal.signal(signal.SIGTERM, exit_gracefully)

main_task = Popen(['cockroach', 'start', '--insecure', '--join=master'], shell=False)

while True:
    logging.info('Main loop')  # will not print anything
    time.sleep(5)
